<?php

namespace Drupal\views_block_view_area\Plugin\views\area;


use Drupal\views_block_area\Plugin\views\area\ViewsBlockArea;

/**
 * Provides an area handler which renders a block entity with view in a certain view mode.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_block_view_area")
 */
class ViewsBlockViewArea extends ViewsBlockArea {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    return parent::render($empty);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBlock() {
    if (empty($this->options['block_id'])) {
      return NULL;
    }

    /** @var \Drupal\Core\Block\BlockManagerInterface $block_manager */
    $block_manager = $this->blockManager;

    /** @var \Drupal\Core\Block\BlockPluginInterface $block_instance */
    $block_instance = $block_manager->createInstance($this->options['block_id'], ['view' => $this->view]);

    $plugin_definition = $block_instance->getPluginDefinition();

    // Don't return broken block plugin instances.
    if ($plugin_definition['id'] == 'broken') {
      return NULL;
    }

    // Don't return broken block content instances.
    if ($plugin_definition['id'] == 'block_content') {
      $uuid = $block_instance->getDerivativeId();
      if (!$this->entityRepository->loadEntityByUuid('block_content', $uuid)) {
        return NULL;
      }
    }

    return $block_instance;
  }
}
